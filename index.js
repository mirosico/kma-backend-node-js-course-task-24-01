const app = require('./app.js');
const http = require('http');
require('dotenv').config();

const port = parseInt(process.env.PORT) || 56201;
app.set('port', port);

const server = http.createServer(app);
server.listen(port);
