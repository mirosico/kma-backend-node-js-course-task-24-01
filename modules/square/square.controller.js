
const square = async (request, response) => {
    try {
        const number = parseFloat(request.body);
        if (isNaN(number)) {
            response.status(400).send("Invalid input. Please provide a number.");
            return;
        }
        const res = {
            number: number,
            square: number * number
        }
        response.status(200).json(res);
    } catch (e) {
        console.error(e);
        response.status(500).send();
    }
};



module.exports = {
    square,
}

