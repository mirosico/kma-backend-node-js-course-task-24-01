const express = require('express');
const squareController = require("./square.controller");
const router = express.Router();

router.post('/', squareController.square);

module.exports = router;
