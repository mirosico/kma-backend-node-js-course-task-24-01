const dayjs = require("dayjs");
const isLeapYear = require('dayjs/plugin/isLeapYear')
dayjs.extend(isLeapYear)


const date = async (request, response) => {
    try {
        const { year, month, day } = request.params;

        const date = dayjs(`${year}-${month}-${day}`);

        const difference = Math.abs(dayjs().startOf('day').diff(date, 'day'));

        const weekDay = date.format('dddd');

        const isLeapYear = date.isLeapYear();
        response.status(200).json({
            weekDay,
            isLeapYear,
            difference
        })
    } catch (e) {
        console.error(e);
        response.status(500).send();
    }
};

module.exports = {
    date,
}

