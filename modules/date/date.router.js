const express = require('express');
const dateController = require("./date.controller");
const router = express.Router();

router.get('/:year/:month/:day', dateController.date);

module.exports = router;
