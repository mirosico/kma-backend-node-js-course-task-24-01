const reverse = async (request, response) => {
    try {
        const str = request.body
        const res = str.split('').reverse().join('');
        response.status(200).send(res);
    } catch (e) {
        console.error(e);
        response.status(500).send();
    }
};

module.exports = {
    reverse,
}

