const express = require('express');
const reverseController = require("./reverse.controller");
const router = express.Router();

router.post('/', reverseController.reverse);

module.exports = router;
