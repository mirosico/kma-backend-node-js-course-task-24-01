const express = require('express');
const cors = require('cors')
const bodyParser = require('body-parser');

require('dotenv').config();

const squareRouter = require('./modules/square/square.router');
const reverseRouter = require('./modules/reverse/reverse.router');
const dateRouter = require('./modules/date/date.router');

const app = express();

app.use(cors());
app.use(bodyParser.text());
app.use(express.json());
app.use(express.urlencoded({ extended: false }));


app.use('/square', squareRouter);
app.use('/reverse', reverseRouter);
app.use('/date', dateRouter);

module.exports = app;
